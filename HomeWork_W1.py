# Task 1
import datetime
now = datetime.datetime.now()
print ("Current date and time : ")
print (now.strftime("%Y-%m-%d %H:%M:%S"))
# trả về cụm từ "Current date and time : " và thời gian hiện tại định dạng năm-tháng-ngày giờ:phút:giây

# Task 2
def sum(x, y):
    sum = x + y
    if sum in range(15, 20):
        return 20
    else:
        return sum
print(sum(10, 6))
print(sum(10, 2))
print(sum(10, 12))
# trả về kết quả phép tính tổng với điều kiện nếu tổng trong khoảng (15,20) thì kết quả là 20 nếu không trả về tổng

# Ex1
u, v, x, y, z = 29, 12, 10, 4, 3
# tính u/v
print('u/v = ', u / v)
# tính t với t = (u == v)
t = (u == v)
print('t = ', t)
# tính u % x
print('u%x = ', u % x)
# tính t với t = (x >= y)
t = (x >= y)
print('t = ', t)
# tính u += 5
u += 5
print('u = ', u)
# tính u %= z
u %= z
print('u = ', u)
# tính t = (v > x and y < z)
t = (v > x and y < z)
print('t = ', t)
# tính x**z
print('x**z = ', x**z)
# tính x//z
print('x//z = ', x//z)

# Ex2
import math

r = 5
C = 2 * r * math.pi
A = r**2 * math.pi
print("chu vi = ", C)
print("Diện tích =", A)

# Ex3
s = "Hi John, welcome to python programming for beginner!"
p = "python"
# p có trong s hay không
t = p in s
print(t)
# lây ra từ John
s1 = s[3:7]
print(s1)
# đếm số ký tự o
c = s.count("o")
print("c = ", c)
# đếm số từ có trong s
t = s.split(" ")
l = len(t)
print("l = ", l)

# Ex4
print(
    "Twinkle, twinkle, little star,\n\t How I wonder what you are! \n Up above the world so high,\n\t Like a diamond "
    "in the sky.\n Twinkle, twinkle, little star,\n\t How I wonder what you are.")

# Ex5
l = [23, 4.3, 4.2, 31, 'python', 1, 5.3, 9, 1.7]
# remove từ python
l.remove('python')
print(l)
# sắp xếp list
l.sort()
print(l)

l.sort(reverse=True)
print(l)
# kiểm tra 4.2 có trong chuỗi không
k = 4.2 in l
print(k)